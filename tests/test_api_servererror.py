import pytest
from callpass import ServerStatus
from callpass_server import app, server
from fastapi.testclient import TestClient


@pytest.fixture()
def http_580_client(monkeypatch):
    async def exception_ServerStatus_580(_):
        raise ServerStatus("HTTP 580")

    monkeypatch.setattr(server.lookup, "callsign", exception_ServerStatus_580)

    return TestClient(app)


@pytest.fixture()
def http_990_client(monkeypatch):
    async def exception_ServerStatus_990(_):
        raise ServerStatus("HTTP 990")

    monkeypatch.setattr(server.lookup, "callsign", exception_ServerStatus_990)

    return TestClient(app)


###


def test_code_upstream_http_580(http_580_client):
    response = http_580_client.get("/code/AB3DEF")
    assert response.status_code == 580


def test_code_upstream_http_990(http_990_client):
    response = http_990_client.get("/code/AB3DEF")
    assert response.status_code == 503


###


def test_json_upstream_http_580(http_580_client):
    response = http_580_client.get("/json/AB3DEF")
    assert response.status_code == 580


def test_json_upstream_http_990(http_990_client):
    response = http_990_client.get("/json/AB3DEF")
    assert response.status_code == 503
