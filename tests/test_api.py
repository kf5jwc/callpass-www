from callpass_server import app
from fastapi.testclient import TestClient
from requests.models import Response
from pytest import fixture


@fixture
def client() -> TestClient:
    return TestClient(app)


@fixture
def client_root(client) -> Response:
    return client.get("/")


def test_root_status_code(client_root):
    assert client_root.status_code == 200


def test_root_text(client_root):
    assert "APRS" in client_root.text


@fixture
def client_404(client) -> Response:
    return client.get("/404/page")


def test_404_status_code(client_404):
    assert client_404.status_code == 404
