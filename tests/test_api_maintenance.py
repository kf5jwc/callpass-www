import pytest
import callpass
from callpass_server import app
from fastapi.testclient import TestClient
from requests.models import Response


@pytest.fixture(autouse=True)
def maintenance_mode(monkeypatch):
    def server_updating(_callpass: str) -> dict:
        raise callpass.ServerUpdating

    monkeypatch.setattr(
        callpass.validated_callpass.validated_callpass,
        "fetch_license",
        server_updating,
    )


###


@pytest.fixture
def response_code_maintenance() -> Response:
    client = TestClient(app)
    return client.get("/code/kf5jwc")


def test_maintenance_code_status_code(response_code_maintenance):
    assert response_code_maintenance.status_code == 503


###


@pytest.fixture
def response_json_maintenance() -> Response:
    client = TestClient(app)
    return client.get("/json/kf5jwc")


def test_maintenance_json_status_code(response_json_maintenance):
    assert response_json_maintenance.status_code == 503
