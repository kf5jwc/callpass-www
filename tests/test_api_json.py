from dataclasses import dataclass
import callpass
import callpass_server
from fastapi.testclient import TestClient
from requests.models import Response
import pytest


@pytest.fixture
def client() -> TestClient:
    return TestClient(callpass_server.app)


@pytest.fixture
def client_json_malformed(client) -> Response:
    return client.get("/json/AAA")


def test_json_malformed_status_code(client_json_malformed):
    assert client_json_malformed.status_code == 400


def test_json_malformed_reason(client_json_malformed):
    assert "Malformed" in client_json_malformed.json()["reason"]


@pytest.fixture
def client_json_valid(monkeypatch, client) -> Response:
    @dataclass
    class MockedValidatedCallpass:
        callsign: str
        callpass: int = 5678

        def __post_init__(self):
            self.callsign = "AB3DEF"
            self.callpass = 5678

        def __int__(self) -> int:
            return self.callpass

    monkeypatch.setattr(
        callpass_server.server.lookup, "ValidatedCallpass", MockedValidatedCallpass
    )

    return client.get("/json/AB3DEF")


def test_json_good_status_code(client_json_valid):
    assert client_json_valid.status_code == 200


def test_json_good_callpass(client_json_valid):
    assert client_json_valid.json()["callpass"] == 5678


@pytest.fixture
def client_json_invalid(monkeypatch, client) -> Response:
    def invalid_license(callsign: str):
        raise callpass.InvalidLicense

    monkeypatch.setattr(
        callpass_server.server.lookup, "ValidatedCallpass", invalid_license
    )

    return client.get("/json/AB3DEF")


def test_json_invalid_status_code(client_json_invalid):
    assert client_json_invalid.status_code == 404


@pytest.fixture
def client_json_expired(monkeypatch, client) -> Response:
    def expired_license(*_, **__):
        raise callpass.ExpiredLicense

    monkeypatch.setattr(
        callpass_server.server.lookup, "ValidatedCallpass", expired_license
    )

    return client.get("/json/AB3DEF")


def test_json_expired_status_code(client_json_expired):
    assert client_json_expired.status_code == 403
