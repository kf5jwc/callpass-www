import callpass
import callpass_server
from dataclasses import dataclass
from fastapi.testclient import TestClient
from requests.models import Response
import pytest


@pytest.fixture
def client() -> TestClient:
    return TestClient(callpass_server.app)


@pytest.fixture
def client_code_malformed(client) -> Response:
    return client.get("/code/AAA")


def test_code_malformed_status_code(client_code_malformed):
    assert client_code_malformed.status_code == 400


def test_code_malformed_text(client_code_malformed):
    assert "Malformed" in client_code_malformed.text


@pytest.fixture
def client_code_valid(monkeypatch, client) -> Response:
    @dataclass
    class MockedValidatedCallpass:
        callsign: str
        callpass: int = 5678

        def __post_init__(self):
            self.callsign = "AB3DEF"
            self.callpass = 5678

        def __int__(self) -> int:
            return self.callpass

    monkeypatch.setattr(
        callpass_server.server.lookup, "ValidatedCallpass", MockedValidatedCallpass
    )

    return client.get("/code/AB3DEF")


def test_code_valid_status_code(client_code_valid):
    assert client_code_valid.status_code == 200


def test_code_valid_callpass(client_code_valid):
    assert "5678" in client_code_valid.text


@pytest.fixture
def client_code_invalid(monkeypatch, client) -> Response:
    def invalid_license(callsign: str):
        raise callpass.InvalidLicense

    monkeypatch.setattr(
        callpass_server.server.lookup, "ValidatedCallpass", invalid_license
    )

    return client.get("/code/AB3DEF")


def test_code_invalid_status_code(client_code_invalid):
    assert client_code_invalid.status_code == 404


def test_code_invalid_error(client_code_invalid):
    assert "Invalid" in client_code_invalid.text


@pytest.fixture
def client_code_expired(monkeypatch, client) -> Response:
    def expired_license(*_, **__):
        raise callpass.ExpiredLicense

    monkeypatch.setattr(
        callpass_server.server.lookup, "ValidatedCallpass", expired_license
    )

    return client.get("/code/AB3DEF")


def test_code_expired_status_code(client_code_expired):
    assert client_code_expired.status_code == 403


def test_code_expired_error(client_code_expired):
    assert "Expired" in client_code_expired.text
