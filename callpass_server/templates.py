from collections import namedtuple

from jinja2 import Environment, PackageLoader, select_autoescape

ENV = Environment(
    loader=PackageLoader("callpass_server", "templates"),
    autoescape=select_autoescape(["html"]),
)

TemplateTuple = namedtuple("TemplateTuple", ("root", "code", "maintenance", "error"))

Templates = TemplateTuple(
    root=ENV.get_template("index.html"),
    code=ENV.get_template("code.html"),
    maintenance=ENV.get_template("maintenance.html"),
    error=ENV.get_template("error.html"),
)
