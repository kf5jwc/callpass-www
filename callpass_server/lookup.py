import logging
from callpass import (
    ExpiredLicense,
    InvalidLicense,
    MalformedCallsign,
    ServerStatus,
    ServerUpdating,
    ValidatedCallpass,
)
from pydantic import BaseModel


class StatusBase(BaseModel):
    pass


###


class Ok(StatusBase):
    callsign: str


class Valid(Ok):
    callpass: int


class Invalid(Ok):
    reason: str = "Invalid license!"


class Expired(Invalid):
    reason: str = "Expired license!"


###


class Error(StatusBase):
    reason: str = "Unknown lookup error!"


class Maintenance(Error):
    reason: str = "Server currently updating."


class Malformed(Error):
    reason: str = "Malformed callsign!"


###


async def callsign(callsign: str) -> StatusBase:
    try:
        callpass = ValidatedCallpass(callsign=callsign)

    except MalformedCallsign:
        return Malformed()

    except InvalidLicense:
        return Invalid(callsign=callsign)  # type: ignore

    except ExpiredLicense:
        return Expired(callsign=callsign)  # type: ignore

    except ServerUpdating:
        return Maintenance()

    except ServerStatus as msg:  # pragma: no coverage
        logging.warning("Exception: ServerStatus: %s", msg)  # pragma: no coverage
        raise  # pragma: no coverage

    else:
        return Valid(callsign=callsign, callpass=callpass)  # type: ignore
