import pkg_resources

from fastapi import FastAPI, Form
from fastapi.responses import RedirectResponse, HTMLResponse, JSONResponse
from fastapi.encoders import jsonable_encoder as json
from starlette.staticfiles import StaticFiles
from callpass import ValidatedCallpass, ServerStatus

from . import lookup
from .templates import Templates

app = FastAPI()


@app.get("/", include_in_schema=False)
async def root() -> HTMLResponse:
    return HTMLResponse(
        Templates.root.render(pattern=ValidatedCallpass.callsign_pattern)
    )


@app.post("/code", include_in_schema=False)
async def filter_and_redir(callsign: str = Form("callsign")) -> RedirectResponse:
    return RedirectResponse(
        url=f"/code/{callsign}", status_code=302
    )  # pragma: no cover


@app.get(
    "/code/{callsign}",
    include_in_schema=False,
    responses={
        200: {"model": lookup.Valid, "description": "A callpass will be returned"},
        400: {
            "model": lookup.Malformed,
            "description": "An invalid callsign format was provided",
        },
        403: {"model": lookup.Expired, "description": "The callsign is expired"},
        404: {"model": lookup.Invalid, "description": "The callsign was not found"},
        503: {
            "model": lookup.Maintenance,
            "description": "The server is undergoing an FCC database sync",
        },
    },
)
async def request_code(callsign: str) -> HTMLResponse:
    try:
        result = await lookup.callsign(callsign)
    except ServerStatus as err:
        http_code = int(str(err).split()[-1])
        if http_code not in range(500, 599):
            http_code = 503
        return HTMLResponse(
            Templates.error.render(reason="Server error"), status_code=http_code
        )

    if isinstance(result, lookup.Maintenance):
        return HTMLResponse(Templates.maintenance.render(), status_code=503)

    if isinstance(result, lookup.Malformed):
        return HTMLResponse(Templates.error.render(result), status_code=400)

    if isinstance(result, lookup.Expired):
        return HTMLResponse(Templates.error.render(result), status_code=403)

    if isinstance(result, lookup.Invalid):
        return HTMLResponse(Templates.error.render(result), status_code=404)

    if isinstance(result, lookup.Valid):
        return HTMLResponse(Templates.code.render(result))


@app.get(
    "/json/{callsign}",
    responses={
        200: {"model": lookup.Valid, "description": "A callpass will be returned"},
        400: {
            "model": lookup.Malformed,
            "description": "An invalid callsign format was provided",
        },
        403: {"model": lookup.Expired, "description": "The callsign is expired"},
        404: {"model": lookup.Invalid, "description": "The callsign was not found"},
        503: {
            "model": lookup.Maintenance,
            "description": "The server is undergoing an FCC database sync",
        },
    },
)
async def app_json(callsign: str) -> JSONResponse:
    try:
        result = await lookup.callsign(callsign)
    except ServerStatus as err:
        http_code = int(str(err).split()[-1])
        if http_code not in range(500, 599):
            http_code = 503
        return JSONResponse({"error": "Server error"}, status_code=http_code)

    if isinstance(result, lookup.Maintenance):
        return JSONResponse(json(result), status_code=503)

    if isinstance(result, lookup.Malformed):
        return JSONResponse(json(result), status_code=400)

    if isinstance(result, lookup.Expired):
        return JSONResponse(json(result), status_code=403)

    if isinstance(result, lookup.Invalid):
        return JSONResponse(json(result), status_code=404)

    if isinstance(result, lookup.Valid):
        return JSONResponse(json(result))


app.mount(
    "/css",
    StaticFiles(directory=pkg_resources.resource_filename(__name__, "static/css")),
    name="css",
)
app.mount(
    "/images",
    StaticFiles(directory=pkg_resources.resource_filename(__name__, "static/images")),
    name="images",
)
