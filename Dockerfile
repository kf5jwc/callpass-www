FROM docker.io/python:3.8
EXPOSE 5000

ADD . /tmp/callpass
RUN python -m pip install /tmp/callpass && rm -r /tmp/callpass

CMD ["python", "-m", "uvicorn", "--host", "0.0.0.0", "--port", "5000", "callpass_server:app"]
